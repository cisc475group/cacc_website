import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { ReplaySubject } from 'rxjs';
import { backEndUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url:any = `${backEndUrl}/admins`;
  status: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(
    private _http: HttpClient
  ) { 
    this.status.next(this.getCurLoginAdmin() != null);
  }

  //API

  //
  setAdmin(admin: any){
    console.log(`here is the admin input: ${admin}`)
    if (admin){
      localStorage.setItem('admin', JSON.stringify(admin.admin));
      localStorage.setItem('token', JSON.stringify(admin.access_token));
      this.status.next(true);
    }else {
      localStorage.removeItem('admin');
      localStorage.removeItem('token');
      this.status.next(false);
    }
    console.log(localStorage.getItem('admin') + ", " +localStorage.getItem('token'))
  }

  removeAdmin(){
    this.setAdmin(null);
  }

  getCurLoginAdmin(){
    let admin = JSON.parse(localStorage.getItem('admin'));
    if (!admin || !admin.email || !admin.role){
      //throw new Error("Fail to get current login admin");
      return null;
    }
    return admin;
  }

  getAccessToken(){
    const token = localStorage.getItem('token').replace(/"/gi, ''); //remove the " around token
    return token;
  }

  getCurAdminRole(){
    var curAdmin = this.getCurLoginAdmin();
    if (curAdmin){
      return curAdmin.role;
    }
    return null;
  }
}
