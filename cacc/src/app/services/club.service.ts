import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, throwError, of } from 'rxjs';
import { backEndUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClubService {
  private clubUrl = `${backEndUrl}/club`;

  constructor(
    private http: HttpClient
  ) { }
  private options = {
    responseType: 'json' as const,
  };

  public getAllClubs() {
    return this.http
    .get<any>(
      this.clubUrl, 
      this.options
    ).pipe(
      catchError(error => of (error)),
      map((header: any) => {
        console.log(header);
        return header;
      })
    );
  }

  public getClubByID(id: number) {
    const clubLink = this.clubUrl + '/' + id;
    return this.http.get(clubLink, this.options);
  }


}
