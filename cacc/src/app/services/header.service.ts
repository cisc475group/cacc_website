import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  url:any = `${backEndUrl}/headerMenu`;//'http://localhost:3000/api/headerMenu';

  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(
    private _http: HttpClient,
    private _adminSvc: AdminService
    ) { }

  /* write service to upload or download header elements */

  public getHeaderMenu(

  ){
    return this._http
      .get<any>(
        this.url,
        {headers : this.headers}
      ).pipe(
        catchError(error => of (error)),
        map((header: any) => {
          console.log(header);
          return header;
        })
      )
  }

}
