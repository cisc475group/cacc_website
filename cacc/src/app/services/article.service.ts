import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {of} from 'rxjs';
import { backEndUrl } from 'src/environments/environment';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  url:any = `${backEndUrl}/articles`;//'http://localhost:3000/api/articles';

  private headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  constructor(
    private _http: HttpClient,
    private _adminSvc: AdminService
    ) { }

  /* write service to upload or download article (event) */

  /* GET API*/ 
  public getArticleByCategory(
    category: string
  ){
    return this._http
    .get<any>(
      this.url + `/category/${category}`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (`Error: ${error.error.error}; HTTP: ${error.message}`)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  public getArticleByTitle(
    title: string
  ){
    return this._http
    .get<any>(
      this.url + `/title/${title}`,
      {headers : this.headers}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }


  /* POST API*/ 
  public addArticle(
    title: string,
    content: string,
    category: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`,
      "Content-Type": "application/json",
    });
    return this._http
    .post<any>(
      this.url + `/createArticle`,
      {
        content: content,
        title: title,
        category: category,
        role: this._adminSvc.getCurAdminRole()
      },
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  /*DELETE API*/
  public deleteArticleById(
    id: any
  ){
    const headersWithAuth = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`
    });
    return this._http
    .delete<any>(
      this.url + `/delete/${id}`,
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any) => {
        console.log(header);
        return header;
      })
    );
  }

  /*PUT API */
  public editArticleById(
    id: any,
    title: string,
    content: string,
    category: string
  ){
    const headersWithAuth = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": `Bearer ${this._adminSvc.getAccessToken()}`
    });
    return this._http
    .put<any>(
      this.url + `/edit/${id}`,
      {
        content: content,
        title: title,
        category: category,
        role: this._adminSvc.getCurAdminRole()
      },
      {headers : headersWithAuth}
    ).pipe(
      catchError(error => of (error)),
      map((header:any)=> {
        console.log(header);
        return header;
      })
    )
  }

  /* helper function */
  getImageSources(str: string){
    //console.log(`incomming string: ${str}`);
    let imgsrc = [];
    let result = str.split(/src="/gi);
    var src = "";
    if (result.length >= 2){
      src = result[1].split(/">/gi)[0];
    }
    imgsrc.push(src);
    //console.log(imgsrc);
    return imgsrc;
  }
  
  getContent(str: string){
    let final = "";
    const secrete:string  = "##mysecrtejoin##";
    //console.log(`incomming string: ${str}`);
    let result = str.split(`<figure`).join(secrete);
    result = result.split('</figure>').join(secrete);
    result.split(secrete).forEach((each, index) => {
        if (index != 1){
          //console.log(each);
          final += each;
        }

    })

    return final;
  }


}
