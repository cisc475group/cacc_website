import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule }   from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ReactiveFormsModule } from '@angular/forms';  
// import { GoogleMapsModule } from '@angular/google-maps'

import { HttpClientModule } from '@angular/common/http';

// import Module from app-material
import { AppMaterialModule } from './app-material/app-material.module';
//  import Component from ./components 
import { CarouselComponent } from './components/carousel/carousel.component';
import { InfoChuckComponent } from './components/info-chuck/info-chuck.component';
import { InfoChuckReverseComponent } from './components/info-chuck-reverse/info-chuck-reverse.component';
import { DividingLineComponent } from './components/dividing-line/dividing-line.component';
import { HeadbarComponent } from './components/headbar/headbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { LatestEventComponent } from './components/latest-event/latest-event.component';
import { InfoChuckTwoComponent } from './components/info-chuck-two/info-chuck-two.component';
import { EventCardComponent } from './components/event-card/event-card.component';
import { EventCardPlainComponent } from './components/event-card-plain/event-card-plain.component';
import { CkeditorComponent } from './components/ckeditor/ckeditor.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { SchoolCardComponent } from './components/school-card/school-card.component';
import { HeaderEditorComponent } from './components/header-editor/header-editor.component';

// import component from ./pages
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ClubMainPageComponent } from './pages/club-main-page/club-main-page.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { SchoolMainPageComponent } from './pages/school-main-page/school-main-page.component';
import { SummerCampComponent } from './pages/summer-camp/summer-camp.component';
import { EditorPageComponent } from './pages/editor-page/editor-page.component';
import { ClubPageComponent } from './pages/club-page/club-page.component';
import { CaccmontComponent } from './pages/caccmont/caccmont.component';
import { ChineseschooldeComponent } from './pages/chineseschoolde/chineseschoolde.component';
import { ChunhuischoolComponent } from './pages/chunhuischool/chunhuischool.component';
//import { AboutUsComponent } from './pages/about-us/about-us.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { EventPageComponent } from './pages/event-page/event-page.component';
import { ArticleComponent } from './components/article/article.component';


 








FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  listPlugin,
  interactionPlugin
])


@NgModule({
  declarations: [
    AppComponent,
    HeadbarComponent,
    HomePageComponent,
    CarouselComponent,
    FooterComponent,
    InfoChuckComponent,
    InfoChuckReverseComponent,
    DividingLineComponent,
    ClubMainPageComponent,
    LatestEventComponent,
    ContactUsComponent,
    SchoolMainPageComponent,
    InfoChuckTwoComponent,
    EventCardComponent,
    SummerCampComponent,
    EventCardPlainComponent,
    CkeditorComponent,
    EditorPageComponent,
    CalendarComponent,
    ClubPageComponent,
    CaccmontComponent,
    SchoolCardComponent,
    ChineseschooldeComponent,
    ChunhuischoolComponent,
    //AboutUsComponent,
    LoginPageComponent,
    EventPageComponent,
    HeaderEditorComponent,
    ArticleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    MDBBootstrapModule,
    FormsModule,
    CKEditorModule,
    FullCalendarModule,
    ImageCropperModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD_TVtbXqRujigrvzXphLPf33n_kLxI1QM'
    }),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
