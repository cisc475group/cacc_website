import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ClubMainPageComponent } from './pages/club-main-page/club-main-page.component';
import { SchoolMainPageComponent } from './pages/school-main-page/school-main-page.component';
import { SummerCampComponent } from './pages/summer-camp/summer-camp.component';
import {EditorPageComponent} from './pages/editor-page/editor-page.component';

import { ClubPageComponent } from './pages/club-page/club-page.component';
import { CaccmontComponent } from './pages/caccmont/caccmont.component';
import { ChineseschooldeComponent } from './pages/chineseschoolde/chineseschoolde.component';
import { ChunhuischoolComponent } from './pages/chunhuischool/chunhuischool.component';
//import { AboutUsComponent } from './pages/about-us/about-us.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { EventPageComponent } from './pages/event-page/event-page.component';

// import auth guard
import { AuthGuard } from './security/auth.guard';

const routes: Routes = [
  {path: '', component:HomePageComponent},
  {path: 'clubs', component: ClubMainPageComponent},
  {path: 'contact-us',component:ContactUsComponent},
  {path: 'school-main',component:SchoolMainPageComponent},
  {path: 'summer-camp',component:SummerCampComponent},
  {path: 'editor', component: EditorPageComponent, canActivate:[AuthGuard]},
  {path: 'clubs/:id', component: ClubPageComponent},
  {path: 'school/caccmont', component: CaccmontComponent},
  {path: 'school/chineseschoolde', component: ChineseschooldeComponent},
  {path: 'school/chunhuischool', component: ChunhuischoolComponent},
  //{path: 'about-us', component: AboutUsComponent},
  {path: 'login', component: LoginPageComponent},
  {path: 'event', component: EventPageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
