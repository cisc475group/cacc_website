import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from '../../../../node_modules/jquery';
import { HeaderService } from '../../services/header.service';
import {
  SlideInOutAnimation
} from './animations';

// example for the json object
interface menuElement{
    name: string; 
    link: string;
    subElement: Array<menuElement>;
}
const DONATE_LINK = 'https://www.paypal.com/donate?token=TR-OWpk6Spcv-7C85WcspgVl0BOL93FsvRyTMLB_i6n6955z3hd2nEFb-1sXPz5mgGpQdRtApdDFR9v-';
const CACC_REGISTER = 'https://member.caccdelaware.info/';

@Component({
  selector: 'app-headbar',
  templateUrl: './headbar.component.html',
  styleUrls: ['./headbar.component.scss'],
  animations: [SlideInOutAnimation]
})
export class HeadbarComponent implements OnInit {
  constructor(
    private renderer: Renderer2,
    private router: Router,
    private _headerSvc: HeaderService
    ) { }

  isLoggedIn: boolean = false;
  regularSize: boolean = true;
  visibility: string = "";
  visibilityMenu: string = "";
  animationState: string;
  menuTerm: string[];
  menuHtml: string = '';

  menuExapmple: Array<menuElement> =[
    {
      name: "Home",
      link: '',
      subElement: []
    },
    {
      name: "Clubs",
      link: '/clubs',
      subElement: []
    },
    {
      name: "Events",
      link: '/event',
      subElement: []
    },
    {
      name: "Schools",
      link: 'none',
      subElement: [
        {
          name: "CACC MONTESSORI SCHOOL",
          link: 'school/caccmont',
          subElement: []
        },
        {
          name: "CHINESE SCHOOL OF DELAWARE 德立華中文學校",  //德立華中文學校
          link: 'school/chineseschoolde',
          subElement: []
        },
        {
          name: "CHUN-HUI CHINESE SCHOOL 春晖中文学校",   //春晖中文学校
          link: 'school/chunhuischool',
          subElement: []
        }
      ]
    },
    {
      name: "Summer Camp",
      link: '/summer-camp',
      subElement: []
    },
    {
      name: "Contact Us",
      link:'/contact-us',
      subElement:[]
    },
    {
      name: "Join Us",
      link: CACC_REGISTER,
      subElement: []
    },
    {
      name: "Donate",
      link: DONATE_LINK,
      subElement:[]
    }

  ];

  @ViewChild('dynamicMenu') menudiv: ElementRef;
  @ViewChild('2222') sideBar: ElementRef;
  @ViewChild('menu') sideMenu: ElementRef;

  ngOnInit(): void {
    this.animationState = "out";
    this.checkWindowSize();
    this.onWindowSizeChanged();
    this.onScrolling();
    console.log(this.menuExapmple);
    
    
    this._headerSvc.getHeaderMenu().subscribe(header => {
      console.log(header.data);
    });
  }

  ngAfterViewInit(){
    //this.renderMenu(this.menuExapmple, this.menudiv.nativeElement, "headerBtn subNavBtn");
    

  }

  onHeadBtnClick(link: string){
    if (link == "none"){
      return;
    }

    if (link == DONATE_LINK || CACC_REGISTER){
      window.location.href= link;
      return;
    }
    //window.location.href= link;
    this.router.navigate([link]);
  }

  toggleShowDiv(element: any){
    if (element){
      if (element.link == "none") return;
      if (element.link == 'https://member.caccdelaware.info/'){
        window.location.href = element.link;
        return;
      }
      this.router.navigate([element.link]);
    }

    this.animationState = this.animationState === 'out' ? 'in' : 'out';
    console.log(this.animationState);
    
  }

  onWindowSizeChanged(){
    let self = this;
    window.addEventListener("resize", function(){
      self.checkWindowSize();
    });
  }

  checkWindowSize(){
    let self = this;
    if (window.innerWidth <= 1173){
      //console.log("not regular size ");
      self.regularSize = false;
      this.visibility = "none";
      this.visibilityMenu = "block"
    }else {
      //console.log("regular size");
      self.regularSize = true;
      this.visibility = "block";
      this.visibilityMenu = "none";
      self.animationState = "out";
    }
  }

  onScrolling(){
    this.renderer.listen('window', 'scroll', (e) => {
      this.animationState = 'out';
    });
  }

}
