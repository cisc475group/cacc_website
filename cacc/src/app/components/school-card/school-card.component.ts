import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-school-card',
  templateUrl: './school-card.component.html',
  styleUrls: ['./school-card.component.scss']
})
export class SchoolCardComponent implements OnInit {
  @Input() imgSources : string[];
  @Input() title : string;
  @Input() article : string;
  @Input() link : string;
  
  constructor() { }

  ngOnInit(): void {
    if (!this.imgSources){
      console.log("ERROR: FAIL TO LOAD IMGSOURCE");
      return;
    }
  }

linkFunc(){
  window.open(this.link);
}

}
