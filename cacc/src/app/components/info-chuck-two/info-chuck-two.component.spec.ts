import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoChuckTwoComponent } from './info-chuck-two.component';

describe('InfoChuckTwoComponent', () => {
  let component: InfoChuckTwoComponent;
  let fixture: ComponentFixture<InfoChuckTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoChuckTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoChuckTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
