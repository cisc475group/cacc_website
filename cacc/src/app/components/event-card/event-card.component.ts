import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {
  
  @Input() eventName: string;
  @Input() month: string;
  @Input() date: string;
  @Input() time: string;
  @Input() endTime: string;
  @Input() imgSource: string;

  constructor() { }

  ngOnInit(): void {
    // this.eventName = "EVENT NAME";
    // this.month = "Oct";
    // this.date = "19";
    // this.time = "12pm";
    // this.imgSource = "https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg";
  }

}
