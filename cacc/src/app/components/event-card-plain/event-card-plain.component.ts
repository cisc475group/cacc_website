import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-event-card-plain',
  templateUrl: './event-card-plain.component.html',
  styleUrls: ['./event-card-plain.component.scss']
})
export class EventCardPlainComponent implements OnInit {

  constructor() { }
  @Input() date: Date;
  @Input() club: string;
  @Input() title: string;
  @Input() description: string;
  
  ngOnInit(): void {
  }

}
