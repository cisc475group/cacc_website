import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCardPlainComponent } from './event-card-plain.component';

describe('EventCardPlainComponent', () => {
  let component: EventCardPlainComponent;
  let fixture: ComponentFixture<EventCardPlainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCardPlainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCardPlainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
