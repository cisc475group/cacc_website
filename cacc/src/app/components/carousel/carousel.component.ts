import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  @Input() imgSources : string[];
  firstSlide : string;

  constructor() { }

  ngOnInit(): void {
    if (!this.imgSources){
      console.log("ERROR: FAIL TO LOAD IMGSOURCE");
      return;
    }
    this.firstSlide = this.imgSources[0]
  }

}
