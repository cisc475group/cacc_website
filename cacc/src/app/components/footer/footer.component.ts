import { Component, OnInit, HostListener } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  contactUsLink = '/contact-us';
  joinUsLink = 'https://member.caccdelaware.info';
  donateLink = 'https://www.paypal.com/donate?token=TR-OWpk6Spcv-7C85WcspgVl0BOL93FsvRyTMLB_i6n6955z3hd2nEFb-1sXPz5mgGpQdRtApdDFR9v-';
  adminSigninLink = '/editor';//'/login';
  constructor(
    private _adminSvc: AdminService
  ) { }
 
  ngOnInit(): void {
  }
  
}
