import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../node_modules/jquery';
import {HeaderService} from '../../services/header.service';

@Component({
  selector: 'app-header-editor',
  templateUrl: './header-editor.component.html',
  styleUrls: ['./header-editor.component.scss']
})
export class HeaderEditorComponent implements OnInit {
  editOptionSelection : string[] = [
    "Add", "Edit", "Delete"
  ];
  parentSelection: string[] = [];
  elementSelection: string[] = [];

  editOptionSelector: string = "";
  parentSelector: string = "";
  elementSelector: string = "";

  renderElementSelection: boolean = false;
  renderEditor: boolean = false;
  
  constructor(private _headerSvc: HeaderService) { }

  ngOnInit(): void {
    this.onEditOptionSelectionChanged();
    this.onParentSelectionChanged();
  }

  onEditOptionSelectionChanged(){
    $('#editOptionSelection2').change(() => {
      this.editOptionSelector = $('#editOptionSelection2').val();
      this.readParentOptions();
      if (this.editOptionSelector == "Add"){
        this.renderElementSelection = false;
      }else{
        this.renderElementSelection = true;
      }
    });
  }

  onParentSelectionChanged(){
    $('#parentSelection').change(() => {
      this.parentSelector = $('#parentSelection').val();
      this.readElementOptions();
    });
  }

  onSelect(){
    this.editOptionSelector = $('#editOptionSelection2').val();
    this.parentSelector = $('#parentSelection').val();
    if (this.renderElementSelection == true){
      this.elementSelector = $('#elementSelection2').val();
    }else {
      this.elementSelector = null;
    }

    alert(`select EditOption: ${this.editOptionSelector} -> Parent: ${this.parentSelector} -> Element: ${this.elementSelector}`);

  }

  onSave(){

  }

  readParentOptions(){
    this.parentSelection = [];
    //this.parentSelection.push("none");
    this.parentSelection = [
      "none",
      "Home",
      "Clubs",
      "Events",
      "School1",
      "School2",
      "School3",
      "Summer Camp",
      "About Us"
    ];
  }

  readElementOptions(){
    if (this.parentSelector == 'none'){
      this.elementSelection=[
        "Home",
        "Clubs",
        "Events",
        "School1",
        "School2",
        "School3",
        "Summer Camp",
        "About Us"
      ];
    }else {
      this.elementSelection = [];
    }
  }

}
