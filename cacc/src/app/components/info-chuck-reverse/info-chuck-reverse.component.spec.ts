import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoChuckReverseComponent } from './info-chuck-reverse.component';

describe('InfoChuckReverseComponent', () => {
  let component: InfoChuckReverseComponent;
  let fixture: ComponentFixture<InfoChuckReverseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoChuckReverseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoChuckReverseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
