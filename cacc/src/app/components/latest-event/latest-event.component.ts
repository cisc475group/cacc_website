import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-latest-event',
  templateUrl: './latest-event.component.html',
  styleUrls: ['./latest-event.component.scss']
})
export class LatestEventComponent implements OnInit {
  hasHoveredOn: boolean;

  
  constructor() { }

  ngOnInit(): void {
    this.hasHoveredOn = false;
    this.handleFirstNewsDisplay();
  }


  handleFirstNewsDisplay(){
    if (!this.hasHoveredOn){
      document.getElementById('firstNewsChild').style.display="block";
      document.getElementById('firstNews').style.backgroundColor = "rgba(184, 20, 20, 0.555)";
    }else{
      document.getElementById('firstNewsChild').style.display="none";
      document.getElementById('firstNews').style.backgroundColor = "transparent";
    }
  }

  handleMouseHover(){
    this.hasHoveredOn = true;
    this.handleFirstNewsDisplay();
  }

  handleMouseLeave(){
    this.hasHoveredOn = false;
    this.handleFirstNewsDisplay();
  }
}
