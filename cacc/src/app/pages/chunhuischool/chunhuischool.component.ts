import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chunhuischool',
  templateUrl: './chunhuischool.component.html',
  styleUrls: ['./chunhuischool.component.scss']
})
export class ChunhuischoolComponent implements OnInit {
  title : string = "CHUN-HUI CHINESE SCHOOL 春晖中文学校";
  article : string = "Founded in September 1996 initially with 43 students, \
  the Delaware Chun Hui Chinese School has now grown to be the largest Chinese school in the State of Delaware with more than 300 students. \
  By exposing our students to the Chinese language and culture through a rich and fun curriculum, \
  the school contributes to their intellectual, cultural, and ethical development in a global and diverse society. \n \
  春晖中文学校成立于一九九六年九月。学校由最初的43名学生，到现在拥有300多名学生，已经发展成为特拉华州最大的中文学校。\
  通过丰富和生动的课 程，寓教于乐的教学方法，让以英语为母语的学生们有学习中文，了解中国文化的机会； 同时也促进了他们的才智、文化和道德的发展。 \n \ ";
                      
  imgSources : string[] = [
    "../../../assets/ChunHuiSchool/chunhui1.jpg",
    "../../../assets/ChunHuiSchool/chunhui2.jpg",
    "../../../assets/ChunHuiSchool/chunhui3.jpg",
  ];

  link : string = "https://chunhuischool.org/";
  constructor() { }

  ngOnInit(): void {
  }

}
