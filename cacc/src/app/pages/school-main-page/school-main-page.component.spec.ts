import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolMainPageComponent } from './school-main-page.component';

describe('SchoolMainPageComponent', () => {
  let component: SchoolMainPageComponent;
  let fixture: ComponentFixture<SchoolMainPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolMainPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
