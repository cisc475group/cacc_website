import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chineseschoolde',
  templateUrl: './chineseschoolde.component.html',
  styleUrls: ['./chineseschoolde.component.scss']
})
export class ChineseschooldeComponent implements OnInit {
  title : string = "CHINESE SCHOOL OF DELAWARE 德立華中文學校";
  article : string = "Chinese School of Delaware (CSD) was established in 1971. \
                      Along with CACC, the school has been recognized as the local resource for Chinese culture and language.\n \
                      CSD adopts a two-track learning system.\
                      Track A is for families who speak Chinese at home (heritage) and track B is for families who do not speak Chinese at home (non-heritage). \
                      Both track students will join together for culture classes. \n \
                      德立華中文學校創立於一九七一年，是一所於週末上課的的私立學校。\
                      學校目前有國語班十六班，採雙軌制。A-Track 適合家裏說華語的學生，B-Track 適合家裏不說華語的學生。\
                      目前有學前班兩班、幼稚園一班、一至十年級)。\
                      本校注重師資培訓並積極鼓勵老師投入主流學校教學，2013-14 學年度，本校七位老師及一位行政人員參加美國大學理事會(College Board)所舉辦之AP中文師資培訓，目前有四位老師任教於主流學校。\n ";
                      
  imgSources : string[] = [
    "../../../assets/ChineseSchoolDe/Chinese_School_DE_1.png",
    "../../../assets/ChineseSchoolDe/Chinese_School_DE_2.png",
    
  ];

  link : string = "http://chineseschoolde.org/";
  constructor() { }

  ngOnInit(): void {
  }


}

