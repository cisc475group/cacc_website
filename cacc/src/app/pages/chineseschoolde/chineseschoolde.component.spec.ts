import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChineseschooldeComponent } from './chineseschoolde.component';

describe('ChineseschooldeComponent', () => {
  let component: ChineseschooldeComponent;
  let fixture: ComponentFixture<ChineseschooldeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChineseschooldeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChineseschooldeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
