import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../node_modules/jquery';
import {EventService} from "../../services/event.service"
@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.scss']
})
export class EventPageComponent implements OnInit {
  
  lastFocusedIndex: number;
  focusedEvent: any;
  focusedInfo: string[];
  eventList: any[];
  carouselImgSources: string[] = [
    "../../../assets/Event/36660313_1498203030285902_6030530053798887424_o.jpg",
    "../../../assets/Event/37648698_1911883112168526_2083630871721541632_n.jpg"
  ];

  constructor(private _eventScv : EventService) { }

  ngOnInit(): void {
    // exammple list
    this.eventList = [
      {
        name: "event1",
        date: "10/31/2020",
        info: "here is event1. Description",
        imgSource: "../../../assets/Event/events1.jpg"
      },
      {
        name: "event2",
        date: "10/31/2020",
        info: "here is event2. Description",
        imgSource: "../../../assets/Event/36660313_1498203030285902_6030530053798887424_o.jpg"
      },
      {
        name: "event3",
        date: "10/31/2020",
        info: "here is event3. Description",
        imgSource: "../../../assets/Event/37648698_1911883112168526_2083630871721541632_n.jpg"
      },
      {
        name: "event4",
        date: "10/31/2020",
        info: "here is event4. Description",
        imgSource: "https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
      },
      {
        name: "event5",
        date: "10/31/2020",
        info: "here is event5. Description",
        imgSource: "https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
      },
      {
        name: "event6",
        date: "10/31/2020",
        info: "here is event6. Description",
        imgSource: "https://mdbootstrap.com/img/Photos/Slides/img%20(68).jpg"
      },
    ];

    this.focusedEvent = this.eventList[0];
    this.lastFocusedIndex = 0;

    this._eventScv.getAllEvents().subscribe(data => {
      this.setEventList(data, ()=> {
        if (this.eventList.length == 0){
          console.log("unable to load eventList");
        }
      });
    })
    
  }

  ngAfterViewInit(){
    this.listItemOnSelected(0);
  }

  setEventList(data: any, callback:()=>void){
    console.log(data);
      if (!data){throw new Error ("data is undefined")}
      if (data.error){
        throw new Error (data.error);
      }
      this.eventList = [];
      data.forEach((each) => {
        this.eventList.push({
          name: each.title,
          date: each.date,
          address: each.address,
          time: each.time,
          endTime: each.endTime,
          category: each.category,
          info: this._eventScv.getContent(each.content),
          imgSource: this._eventScv.getImageSources(each.content)[0]
        })
      })
      this.focusedEvent = this.eventList[0];

      callback();
  }
  toggleEventBtn(index: number){
    // this.focusedEvent = this.eventList[index];
    // alert(`click on ${this.focusedEvent.name}`);
    this.listItemOnSelected(index);

    $("#infoCard").fadeOut(() => {
      this.focusedEvent = this.eventList[index];
      $("#infoCard").fadeIn();
    });
  }

  listItemOnSelected(index: number){
    // deselect first
    $('#eventList').children().eq(this.lastFocusedIndex).css({
      "background-color": "transparent"
    });

    // on select effect
    $('#eventList').children().eq(index).css({
      "background-color": "#56baed"
    });
    this.lastFocusedIndex = index;
  }

  onSearchBtnClicked(){
    let input :string = $("#searchInput").val();
    if (input.includes("'") || input.includes('"')){
      alert("your search input contains invalid character");
      return;
    }

    if (input == ""){
      this._eventScv.getAllEvents().subscribe(data => {
        this.setEventList(data, ()=>{});
      })
    }else {
      this._eventScv.searchEventByTitle(input).subscribe(data => {
        this.setEventList(data, ()=>{
          if (this.eventList.length == 0){
            alert("Unable to find the content you searched");
          }
        });
      })
    }
  }
}
