import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin.service';
import { AuthService } from '../../services/auth.service';
import { ClubService } from '../../services/club.service';
import * as $ from '../../../../node_modules/jquery'; 

// subNav option
const WEB_PAGE_EDITOR = "Web Page Editor",
      REGISTER_CLUB_ADMIN = "Registe Club Admin",
      LOG_OUT = "Log Out";
@Component({
  selector: 'app-editor-page',
  templateUrl: './editor-page.component.html',
  styleUrls: ['./editor-page.component.scss']
})
export class EditorPageComponent implements OnInit {
 
  curAdmin: any;
  curAdminInfo: string;
  subNavElements: string[];
  curNavSelected: string;
  lastSelectedIndex: number;
  roleOption: string[];
  
  constructor(
    private _adminSvc: AdminService,
    private _authSvc: AuthService,
    private _clubSvc: ClubService
  ) { }

  ngOnInit(): void {
    this.getCurLoginAdmin();
    this.initSubNavElements();
    this.getRoleOption();
  }

  ngAfterViewInit(){
    this.listItemOnSelected(0);
  }

  initSubNavElements(){
    if (!this.curAdmin){
      throw new Error("Unable to get the current login admin");
    }
  
    switch(this.curAdmin.role){
      case "admin":{
        this.subNavElements = [WEB_PAGE_EDITOR, REGISTER_CLUB_ADMIN, LOG_OUT];
        break;
      }
      default:{
        this.subNavElements = [WEB_PAGE_EDITOR, LOG_OUT];
        break;
      }
    }
    this.lastSelectedIndex = 0;
    this.curNavSelected = this.subNavElements[this.lastSelectedIndex];
  }

  getCurLoginAdmin(){
    this.curAdmin = this._adminSvc.getCurLoginAdmin();
    if (!this.curAdmin){
      throw new Error("Unable to get the current login admin");
    }
    this.curAdminInfo = `current login admin is: ${this.curAdmin.email}, ${this.curAdmin.role}`
  }

  onSubNavElementClicked(index: any){
    this.listItemOnSelected(index);
    switch (this.subNavElements[index]){
      case WEB_PAGE_EDITOR : {
        this.curNavSelected = WEB_PAGE_EDITOR;
        break;
      }
      case REGISTER_CLUB_ADMIN : {
        this.curNavSelected = REGISTER_CLUB_ADMIN;
        break;
      }
      case LOG_OUT : {
        var confirmation = confirm("Do you want to log out?");
        if (confirmation){
          this.curNavSelected = LOG_OUT;
          this._authSvc.logout();
        }else {

        }
        break;
      }
    }
  }

  listItemOnSelected(index: number){
    if (this.subNavElements[index] == LOG_OUT){return;}

    // deselect first
    $('#subNav').children().eq(this.lastSelectedIndex).css({
      "background-color": "transparent"
    });

    // on select effect
    $('#subNav').children().eq(index).css({
      "background-color": "#56baed"
    });
    this.lastSelectedIndex = index;
  }s

  getRoleOption(){
    this.roleOption = [];
    this._clubSvc.getAllClubs().subscribe(data => {
      data.forEach((each) => {
        this.roleOption.push(each.engname);
      })
    });
  }

  register(){
    let email = $('#email').val();
    let password = $('#password').val();
    let confirmPassword = $('#confirmPassword').val();
    let role = $('#role').val();

    this._authSvc.register(email, password, confirmPassword,role).subscribe(data => {
      if (data.error){
        alert(data.error.error);
      }else {
        alert(data.success);
      }
    })
  }

}
