import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubMainPageComponent } from './club-main-page.component';

describe('ClubMainPageComponent', () => {
  let component: ClubMainPageComponent;
  let fixture: ComponentFixture<ClubMainPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubMainPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
