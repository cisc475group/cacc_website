import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-caccmont',
  templateUrl: './caccmont.component.html',
  styleUrls: ['./caccmont.component.scss']
})
export class CaccmontComponent implements OnInit {
  title : string = "CACC MONTESSORI SCHOOL";
  article : string = "CACC Montessori School \n \
    • Montessori Education- Children ages 6 months to 6 years \n \
    • Focused on Cognitive, Social, Physical Growth \n \
    • Low Student/Teacher Ratio \n \
    • Child Directed \n \
    • Individual Instruction \n \
    • International School Environment \n \
    •Enrichment in Spanish, Chinese, Music, Art, Gym and Library \n \
    • After School Activities in Drama, Soccer, Cooking Club, Tennis, Ballet, Art Class, Yoga and Piano \n \
    • Part Time and Full Day Programs /Before and After Schoo \n \ ";
                      
  imgSources : string[] = [
    "../../../assets/CACCMONT/caccmont1.jpg",
    "../../../assets/CACCMONT/caccmont2.jpg",
    "../../../assets/CACCMONT/caccmont3.jpg"
    
  ];

  link : string = "http://www.caccmont.org/";
  constructor() { }

  ngOnInit(): void {
  }

}
