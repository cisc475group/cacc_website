import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaccmontComponent } from './caccmont.component';

describe('CaccmontComponent', () => {
  let component: CaccmontComponent;
  let fixture: ComponentFixture<CaccmontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaccmontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaccmontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
