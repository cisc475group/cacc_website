import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { ClubService } from '../../services/club.service';
import { EventService } from '../../services/event.service';
import { FileService } from '../../services/file.service';
import { ArticleService } from '../../services/article.service';
import { backEndUrl } from 'src/environments/environment';

@Component({
  selector: 'app-club-page',
  templateUrl: './club-page.component.html',
  styleUrls: ['./club-page.component.scss']
})
export class ClubPageComponent implements OnInit {

  //  ATTRIBUTES
  private clubUrl = `${backEndUrl}`;
  carouselImgSources: string[] = [
    // "../../../assets/ClubMain/chorus3.jpg",
    // "../../../assets/ClubMain/clubs3.jpg",
    // "../../../assets/ClubMain/chorus2.jpg"
  ];

  club = {
    id: -1,
    engname: "Unloaded",
    chiname: "未加载"
  };

  events: any;

  infoChunk = {
    title: "About US",
    article: "Unloaded",
    imgSource: "../../../assets/ClubMain/clubs3.jpg"
}

  constructor(
    private route: ActivatedRoute,
    private clubService: ClubService,
    private eventService: EventService,
    private fileService: FileService,
    private articleService: ArticleService,
  ) { }

  ngOnInit(): void {
    let id = this.route.snapshot.params.id;
    this.getClub(id);
  }

  getClub(clubId) {
    this.clubService.getClubByID(clubId).subscribe((data) => this.clubDataHandler(data));
  }

  clubDataHandler(data) {
    this.club = data[0]
    this.getEvents(this.club.engname);
    this.getFiles(this.club.engname);
    this.getArticle(this.club.engname);
  }

  getEvents(clubEngName) {
    this.eventService.getEventsByCategory(clubEngName).subscribe((data) => this.events = data);
  }

  getFiles(clubEngName) {
    this.fileService.getFilesByCategory(clubEngName).subscribe((data) => this.getImagePath(data.data));
  }

  // TODO: FIX address in backend not in font end issue
  getImagePath(data) {
    let re = /apiuploads/gi; 
    data.forEach(element => {
      let tempPath = this.clubUrl + element.path;
      let path = tempPath.replace(re, "static");
      this.carouselImgSources.push(path);
    });
  }

  getArticle(clubEngName) {
    this.articleService.getArticleByCategory(clubEngName).subscribe((data) => {
      if (data.length < 1){
        console.log("No Article found");
      } else {
        this.infoChunk.title = data[0].title;
        this.infoChunk.article = this.articleService.getContent(data[0].content);
        this.infoChunk.imgSource = this.articleService.getImageSources(data[0].content)[0];
      }
    }); 
  }
}
