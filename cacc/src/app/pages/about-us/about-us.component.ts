import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  infoChucks: any[] = [
    {
      title: "ABOUT US",
      article: "Chinese American Community Center (CACC, 德立華華美聯誼中心), \
                in Hockessin, Delaware, is a non-profit, non-sectarian,\
                not partisan and independent organization founded in 1982 by a group of interested Chinese Americans in the Greater Wilmington\
                area which encompasses Delaware, southeastern Pennsylvania, and southern New Jersey. CACC is an affiliate agency of the United Way of Delaware.",
      imgSource: "../../../assets/AboutUs/aboutus3.jpg"
    },
    {
      title: "OUR MISSION",
      article: "The primary mission of CACC is to promote the exchange and integration of Chinese and American cultures\
                by coordinating activities and events throughout the year and by providing a location for various community organizations and clubs to meet. \n \
                CACC is a resource center for its members and the people in the public who are interested in Chinese culture and language.\
                The mission of the CACC is to promote the exchange and integration of Chinese\
                and American cultures through the cultural and educational programs that we offer and through community service. \
                In this way, CACC is able to inspire understanding and appreciation of the Chinese heritage, \
                to encourage members to participate in community services and become an integral part of the mainstream in Delaware.",
      imgSource: "../../../assets/AboutUs/aboutus2.jpg"
    },
    {
      title: "OUR PROGRAMS AND ACTIVITIES",
      article: "CACC operates Montessori School and Child Care Center on weekdays. \
                It hosts two Chinese Schools on weekends which teach kids and teens Chinese language and culture during the school year. \
                CACC sponsors many regular club activities, workshops, classes, seminars, holidays celebrations, concerts, Chinese Culture Camp\
                and Chinese Festival. \n \
                The membership of CACC consists of individuals and families with diverse background and professions. \
                For further information about CACC, please contact Center Manager at 302-239-0432. \n \
                Check us out on Facebook: www.Facebook.com/caccDelaware \n \
                Chinese American Community Center (CACC) \n \
                1313 Little Baltimore Rd \n \
                Hockessin, DE 19707-9701 \n \
                (302) 239-0432 \n \
                United Way code: 9309 \n",
      imgSource: "../../../assets/AboutUs/aboutus4.jpg"
    },
    {
      title: "CACC BOARD OF DIRECTORS",
      article: "Period: April 2018 to March 2019 \n \
                \n \
                \n \
                Officers: \n \
                \n \
                Barbara Silber 蕭齊, Chair 主席 \n \
                Naxin Cai 蔡納新, Vice Chair 副主席 \n \
                Jun Sun 孙骏, Secretary 秘書 \n \
                Tiffany Zhan 詹雅, Treasurer 財務 \n \
                \n \
                Directors 理事: \n \
                \n \
                Ivan Sun 孫懿賢, Director \n \
                Fancia Tang 湯竹芬, Director \n \
                Jack Tsai 蔡維介, Director \n \
                Ping Xu 徐平, Director \n ", 
      imgSource: "../../../assets/AboutUs/aboutus1.jpg"
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
