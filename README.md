## 📑 Documentation:
    Project Front End 
    -  University of Delaware, FALL 2020, CISC475 Advanced Software Engineering, Group 9 Final Project. 
    -  Project: re-build Chinese American Community Center (CACC) Website.
    -  Client: Chinese American Community Center (CACC), Naxin Cai. 
    -  Development Group Member: Yixiong Wu, Letian Zhang, Sicheng Tian, Long Zheng, Xing Gao. 
    -  Instructor: Greg Silber, Teaching Assistant: Vinit Singh. 

<pre>Development tool: 
    Front End: Angular 
    Back End: Node.js 
    DateBase: MySQL 
</pre>

## 🚀 Installation:
For the front end, we used Angular as the framework. Therefore, for keeping working on the project, you should install the Angular first. <br/>
<a href="https://angular.io/docs">For more information about Angular, Click here &nbsp;&nbsp;▶</a>

#### 1. Install Latest Version of Angular Cli in the local repository
```sh
npm install -g @angular/cli
```

#### 2. CD into CACC_webstite/CACC folder and use npm install / npm update to update your module
```sh
npm install
```

```sh
npm update
```

#### 3. Run the program
```sh
ng serve
```

#### 4. After successfully complied, in the printout comment, find your localhost URL. Copy and paste it into your browser.
```sh
http://localhost:XXXX/
```

## ✍ Add new Component/Page/Service/Security:
You should mainly work on four parts under src/app, components, pages, and services. <br/>
- Pages means every single webpage, such as Home page, and Clube page.
- Components means every small components that builds up every page, such as Carousal, and calender.
- Services means service file that calls backend api.
- Security is the part that authorizes router.


### Generate new Page/Component
Each Pages and Components folder contains four file: .html .scss .spec.ts .ts <br/>
Using page and component are just naming, and organized strategy to separate them, the way of generating them are the same. <br/>
```sh
ng g c XXXX
```
Then, you will see a new folder named XXXX under src/app. If it is a page, move it under src/app/pages, otherwise, src/app/components. <br/>
Also, you neeed to make the following changes: <br/>

- In src/app/app.module.ts, the new component/page will be generated under "// import component from ./pages." <br/>
If it is a new component, move it to under "// import component from ./components." This is for easy-documentation.
- In src/app/app-routing.module.ts, if it is a new page, copy the line of import from src/app/app.module.ts and paste it here. <br/>
It will look like this "import { XXXXComponent } from './pages/XXXX/XXXX.component'; "
- In src/app/app-routing.module.ts, if it is a new page, add a new path under the Routes.  <br/>
It will look like this "{path: 'XXXX', component: XXXXComponent},"

### Generate new Service
```sh
ng g s XXXX
```
This will generate two file, XXXX.service.spec.ts and XXX.service.ts. Move both of them into src/app/services.

### Generate new Security
```sh
ng g guard XXXX
```
This will generate two file, XXXX.security.spec.ts and XXX.security.ts. Move both of them into src/app/security. 

## 🔎 Current Issue

##### editor-page: 
Cannot change Carousal image through the editor. The uploaded images are always two large. 
##### club-page: 
- Shape of the Carousal in each club page are quite weird. This need to be fixed.
- Club page only loads 1 article from the backend, if club managers want to publish multiple articles, this need to be fixed.
- Calender components cannot connect to the backend. For the calender, we used an open source one, [FullClendar](https://fullcalendar.io/).
##### club-main-page: 
- Calender components cannot connect to the backend. For the calender, we used an open source one, [FullClendar](https://fullcalendar.io/).
- Recent Events should be loaded from backend.
- Carousal image should be loaded from backed.
##### event-page: 
Searching feature is not secure. May be hacked by SQL injection.
##### contact-us: 
"Check here to receive email updates" feature is not implemented. Intended to work as a subscription to receive email updates of the CACC events.
##### headbar and footer: 
- When you shrink the width of the browser, the most right icon on headbar will be hidden (start at 1200px). Our currnt solution is to change it to cellphone mode at 1200px, which is not really useable. You should work on the cellphone end.
- Paypal link for donate button is invalid. Need to include Paypal Api. 


#####  background for all page: 
The client wants to use Chinese landscape painting as the background as all page. See our design for three school page. Also, we include a  demo in club main page. Current issue is that what we used were free resources, which has low resolution and poor quality. We recommend you guys to find some paid resources and figure our how to implement them as the background for every pages. Need to communicate with the client. 


## ⚠️ Notice
- Do not close your console when you are making changes. Every time you save your changing of the code, it will automatically re-run for you.
- Remember to update your module every time you add or change anything, especially you find any online components resources, such as bootstrap.
- <strong>You can change the backend URL in src/enviroments/enviroments.ts </strong>
- In src/app/pages and src/app/components, you may find somethings are not being used. This is due to version changed required by the client. We saved every components for future possible needs.
- <strong>Besides the login part, we did not add any security for the website. Thus, you should secure any place that allow typing, such as searching in event page and typing in contact us page. </strong>
- <strong>When you want to install a npm module, remember to use this command line to save it into package.json. </strong>
```sh
npm install  --save
```


## 📝 License
<strong>© 2020 Chinese American Community Center</strong>
